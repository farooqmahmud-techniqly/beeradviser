package com.hfad.beeradviser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Farooq on 8/14/2016.
 */
public final class BeerExpert {
    private final Map<String, List<String>> _beers = new HashMap<>();

    public BeerExpert() {
        createMap();
    }

    List<String> getBrands(String color) {
        return _beers.get(color);
    }

    private void createMap() {
        List<String> list = new ArrayList<>();
        list.add("Jack Amber");
        list.add("Red Moose");

        _beers.put("amber", list);

        list = new ArrayList<>();
        list.add("Jail Pale Ale");

        _beers.put("light", list);
        _beers.put("brown", list);
        _beers.put("dark", list);

    }

}
